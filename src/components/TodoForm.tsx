import React from "react";
import {iTodo} from "../interfaces";

type Props = {
  todos: iTodo[]
  setTodos: any
  title: string
  setTitle: any
}

export const TodoForm = ({todos, setTodos, title, setTitle}: Props) => {
  const addTask = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter' && title.length>0 ) {
      let newTask: iTodo = {
        title: title,
        id: Date.now(),
        completed: false
      }
      setTodos( [...todos, newTask])
      setTitle('')
      console.log(todos);
    }
  }

  return (
    <div className='todo-form'>

      <input type="text" id='title' placeholder='Enter the name of task'
             value={title}
             onChange={event => setTitle(event.target.value)}
             onKeyPress={addTask}/>

    </div>
  )
}