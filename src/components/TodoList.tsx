import React, {useMemo} from "react";
import {iTodo} from '../interfaces'

type Props = {
  todos: iTodo[]
  setTodos: Function
  filter: string
  setFilter: Function
}

export const Todolist = ({todos, setTodos, filter, setFilter}: Props) => {

  const filtered = useMemo(()=> {
    if (filter === 'done'){
      return todos.filter(todo=> todo.completed === true)
    }
    if (filter === 'todo'){
      return todos.filter(todo=> todo.completed === false)
    }
    return todos

  },[todos, filter])

  const removeTodo = (todoId: number) => {
    const newTodos = todos.filter(todo => todo.id !== todoId)
    setTodos(newTodos)
  }

  const checked = (completed: boolean, todoId: number) => {
    console.log(completed, todoId)
    const newTodos = todos.map(todo => todo.id === todoId ? {...todo, completed} : todo)
    setTodos(newTodos)
  }

  if (todos.length === 0) {
    return <p>No tasks yet</p>
  }
  return (
    <ul>
      {filtered.map(filteredTodo => {
        return (
          <li key={filteredTodo.id}>
            <label htmlFor="">
              <input type="checkbox"
                     checked={filteredTodo.completed}
                     onChange={event => checked(event.target.checked, filteredTodo.id)}
              />

              <span>{filteredTodo.title}</span>
              <i className='material-icons red-text'
                 onClick={event => removeTodo(filteredTodo.id)}>delete</i>
            </label>
          </li>
        )
      })}
    </ul>
  )
}