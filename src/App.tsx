import React, {useState, useEffect} from 'react';
import {Todolist} from "./components/TodoList";
import {iTodo} from './interfaces'
import {TodoForm} from "./components/TodoForm";

const isTodo = (obj: any): obj is iTodo => typeof obj.completed === 'boolean' && typeof obj.title === 'string'
const isTodoArray = (obj: any): obj is iTodo[] => Array.isArray(obj) && obj.every(isTodo)

function App() {
  const [todos, setTodos] = useState<iTodo[]>([])
  const [title, setTitle] = useState<string>('')
  const [filter, setFilter] = useState<string>('any')

  useEffect(()=>{
    const row = JSON.parse(localStorage.getItem('todos') || '[]')

    if (isTodoArray(row)) setTodos(row)
  },[])

  useEffect(() =>{
    localStorage.setItem('todos', JSON.stringify(todos))
  },[todos])

  return (
    <div className="container">
      <TodoForm todos={todos} setTodos={setTodos} title={title} setTitle={setTitle}/>
      <select value={filter} onChange={event => setFilter(event.target.value)} >
        <option value='any'>Any</option>
        <option value='done'>Done</option>
        <option value='todo'>Todo</option>
      </select>
      <Todolist todos={todos} setTodos={setTodos} filter={filter} setFilter={setFilter}/>
    </div>
  );
}

export default App;
